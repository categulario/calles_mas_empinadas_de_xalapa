use std::io::{Read, Write, BufRead, BufReader};
use std::fs::File;
use std::collections::HashMap;

#[macro_use]
extern crate clap;

use clap::{App, Arg, SubCommand, ArgMatches};
use isahc::prelude::*;

use calles::elevation::{Elevation, LonLat};
use calles::osm::{OsmResponse, OsmElement};
use calles::streets::{SteepestStreets, slope};
use calles::geojson::{feature_collection, feature};
use calles::error::Error;

fn read_elevation_data(filename: &str) -> Result<Vec<(f64, f64, f64)>, Error> {
    let elevation_file = File::open(filename).map_err(|_| Error::ElevationDataFileNotFound)?;
    let reader = BufReader::new(elevation_file);

    Ok(reader.lines().filter_map(|l| l.ok()).map(|line| {
        let pieces: Vec<_> = line.trim().split(' ').map(|p| p.parse().unwrap()).collect();

        (pieces[0], pieces[1], pieces[2])
    }).collect())
}

fn read_osm_data(filename: &str) -> Result<String, Error> {
    let mut osm_file = File::open(filename).map_err(|_| Error::OsmDataFileNotFound)?;
    let mut contents = String::new();

    osm_file.read_to_string(&mut contents).map_err(|_| Error::OsmDataFileReadError)?;

    Ok(contents)
}

fn overpass_subcommand(matches: &ArgMatches) -> Result<(), Error> {
    let bbox: Vec<f64> = matches.values_of("bbox").unwrap().map(|a| a.parse().unwrap()).collect();

    let x1 = bbox[0];
    let y1 = bbox[1];
    let x2 = bbox[2];
    let y2 = bbox[3];

    let body = format!(r#"[out:json][timeout:25];

    // gather results
    (
      way["highway"][highway!~"service|footway|steps|pedestrian|escape|raceway|bridleway|cycleway|path"]({y1},{x1},{y2},{x2});
    );

    // print results
    out body;
    >;
    out skel qt;"#, y1=y1, x1=x1, y2=y2, x2=x2);

    let mut response = isahc::post("http://overpass-api.de/api/interpreter", body).map_err(|_| Error::OverpassNetworkError)?;
    let out_filename = matches.value_of("OUTPUT").unwrap();

    response.copy_to_file(out_filename).map_err(|_| Error::OverpassIOError)?;

    Ok(())
}

fn explore_subcommand(matches: &ArgMatches) -> Result<(), Error> {
    let elevation_filename = matches.value_of("ELEVATION_DATA").unwrap();
    let elevation_data = read_elevation_data(elevation_filename)?;
    let elevation = Elevation::from_utm(14, elevation_data);

    println!("Hay {} datos de elevación", elevation.len());

    let osm_filename = matches.value_of("OSM_DATA").unwrap();
    let osm_data = read_osm_data(osm_filename)?;
    let osm_struct: OsmResponse = serde_json::from_str(&osm_data).map_err(|_| Error::OsmDataDecodingError)?;

    let mut nodes = HashMap::new();
    let mut ways = Vec::new();

    for element in osm_struct.elements {
        match element {
            OsmElement::Way(way) => ways.push(way),
            OsmElement::Node(node) => {
                nodes.insert(node.id, LonLat::new(node.lon, node.lat));
            },
        }
    }

    println!("There are {} nodes and {} ways", nodes.len(), ways.len());

    let mut ss = SteepestStreets::new(matches.value_of("NUM_STREETS").unwrap().parse().unwrap());

    for way in ways {
        if let Some(s) = slope(&way, &nodes, &elevation) {
            ss.push(s, way);
        } else {
            println!("Warning: street without slope: {}", way.id);
        }
    }

    let features: Vec<_> = ss.into_sorted_vec().into_iter().enumerate().map(|(i, (slope, way))| {
        println!("# {}. {}. {} {}.", i + 1, slope, way.id, way);

        feature(way, &nodes)
    }).collect();

    let geojson = feature_collection(features);

    let out_filename = matches.value_of("OUTPUT").unwrap();
    let mut out_file = File::create(out_filename).map_err(|_| Error::OutFileCreationError)?;

    out_file.write_all(geojson.dump().as_bytes()).map_err(|_| Error::OutFileWriteError)?;

    Ok(())
}

fn main() {
    let app = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .subcommand(
            SubCommand::with_name("overpass")
                .about("Downloads the OSM data needed for the algorithm")
                .arg(
                    Arg::with_name("bbox")
                        .help("BBOX of the area to download in x1, y1, x2, y2 format")
                        .required(true)
                        .short("b")
                        .long("bbox")
                        .takes_value(true)
                        .multiple(true)
                        .number_of_values(4)
                        .require_delimiter(true)
                        .allow_hyphen_values(true)
                )
                .arg(
                    Arg::with_name("OUTPUT")
                        .help("Where to write all the data to")
                        .required(true)
                        .short("o")
                        .long("output")
                        .takes_value(true)
                )
        )
        .subcommand(
            SubCommand::with_name("explore")
                .about("Runs the algorithm that finds the steepest streets")
                .arg(
                    Arg::with_name("ELEVATION_DATA")
                        .short("e")
                        .long("elevation")
                        .help("Use this file as source for elevation data in xyz format")
                        .required(true)
                        .takes_value(true)
                )
                .arg(
                    Arg::with_name("OSM_DATA")
                        .long("osm")
                        .help("Path to the downloaded OSM file (see overpass subcommand)")
                        .required(true)
                        .takes_value(true)
                )
                .arg(
                    Arg::with_name("NUM_STREETS")
                        .short("n")
                        .long("num")
                        .help("How many steepest streets to find")
                        .required(false)
                        .takes_value(true)
                        .default_value("13")
                )
                .arg(
                    Arg::with_name("OUTPUT")
                        .help("Where to write all the data to")
                        .required(true)
                        .short("o")
                        .long("output")
                        .takes_value(true)
                )
        );

    let matches = app.get_matches();

    let result = match matches.subcommand() {
        ("overpass", Some(matches)) => overpass_subcommand(matches),
        ("explore", Some(matches)) => explore_subcommand(matches),
        _ => Ok(println!("{}", matches.usage())),
    };

    if let Err(e) = result {
        eprint!("{}", e);

        std::process::exit(1);
    }
}
