use std::f64::consts::PI;

use rstar::{RTree, RTreeObject, AABB, PointDistance, Point};

/// When querying for the altitude of a point it is used the altitude of the
/// closest point in the Elevation struct. If the closest point is farther
/// than this number then None is returned instead.
const MAX_DISTANCE: f64 = 10.0;

/// Earth radius WGS84
const EARTH_RADIUS: f64 = 6378137.0;

struct ElevatedPoint {
    altitude: f64,
    coords: LonLat,
}

impl ElevatedPoint {
    pub fn new(altitude: f64, coords: LonLat) -> ElevatedPoint {
        ElevatedPoint {
            altitude, coords,
        }
    }
}

impl RTreeObject for ElevatedPoint {
    type Envelope = AABB<LonLat>;

    fn envelope(&self) -> Self::Envelope {
        AABB::from_point(self.coords)
    }
}

impl PointDistance for ElevatedPoint {
    /// Calculate the great circle distance between two points
    /// on the earth (specified in decimal degrees)
    fn distance_2(&self, point: &LonLat) -> f64 {
        self.coords.distance_2(point)
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct LonLat {
    lon: f64,
    lat: f64
}

impl LonLat {
    pub fn new(lon: f64, lat: f64) -> LonLat {
        LonLat {
            lon, lat,
        }
    }

    pub fn to_v(self) -> Vec<f64> {
        vec![self.lon, self.lat]
    }
}

impl From<[f64; 2]> for LonLat {
    fn from([lon, lat]: [f64; 2]) -> LonLat {
        LonLat {
            lon, lat,
        }
    }
}

impl From<&[f64; 2]> for LonLat {
    fn from([lon, lat]: &[f64; 2]) -> LonLat {
        LonLat {
            lon: *lon,
            lat: *lat,
        }
    }
}

impl Point for LonLat {
    type Scalar = f64;

    const DIMENSIONS: usize = 2;

    fn generate(generator: impl Fn(usize) -> Self::Scalar) -> Self {
        LonLat {
          lon: generator(0),
          lat: generator(1),
        }
    }

    fn nth(&self, index: usize) -> Self::Scalar {
        match index {
          0 => self.lon,
          1 => self.lat,
          _ => unreachable!()
        }
    }

    fn nth_mut(&mut self, index: usize) -> &mut Self::Scalar {
        match index {
          0 => &mut self.lon,
          1 => &mut self.lat,
          _ => unreachable!()
        }
    }
}

pub struct Elevation {
    points: RTree<ElevatedPoint>,
}

impl Elevation {
    pub fn altitude(&self, coord: LonLat) -> Option<f64> {
        self.points
            .nearest_neighbor(&coord)
            .filter(|&p| p.coords.distance_2(&coord) < MAX_DISTANCE)
            .map(|p| p.altitude)
    }

    pub fn len(&self) -> usize {
        self.points.size()
    }

    pub fn is_empty(&self) -> bool {
        self.points.size() == 0
    }

    pub fn from_lon_lat_z<I: IntoIterator<Item=(f64, f64, f64)>>(iter: I) -> Elevation {
        let points = RTree::bulk_load(
            iter.into_iter()
                .map(|(x, y, a)| ElevatedPoint::new(a, [x, y].into()))
                .collect()
        );

        Elevation {
            points,
        }
    }

    pub fn from_utm<I: IntoIterator<Item=(f64, f64, f64)>>(zone: i32, iter: I) -> Elevation {
        let points = RTree::bulk_load(
            iter.into_iter()
                .map(|(x, y, a)| ElevatedPoint::new(a, utm_to_lon_lat(x, y, zone)))
                .collect()
        );

        Elevation {
            points,
        }
    }
}

fn utm_to_lon_lat(x: f64, y: f64, zone: i32) -> LonLat {
    let d = 0.9996; // Point scale factor
    let r = EARTH_RADIUS;
    let d2: f64 = 0.00669438;

    let d4 = (1.0 - (1.0 - d2).sqrt()) / (1.0 + (1.0 - d2).sqrt());
    let d15 = x - 500000.0;
    let d11 = ((zone - 1) * 6 - 180) + 3;

    let d3 = d2 / (1.0 - d2);
    let d10 = y / d;
    let d12 = d10 / (r * (1.0 - d2 / 4.0 - (3.0 * d2 * d2) / 64.0 - (5.0 * d2.powi(3)) / 256.0));
    let d14 = d12
        + ((3.0 * d4) / 2.0 - (27.0 * d4.powi(3)) / 32.0) * (2.0 * d12).sin()
        + ((21.0 * d4 * d4) / 16.0 - (55.0 * d4.powi(4)) / 32.0) * (4.0 * d12).sin()
        + ((151.0 * d4.powi(3)) / 96.0) * (6.0 * d12).sin();
    let d5 = r / (1.0 - d2 * (d14).sin() * (d14).sin()).sqrt();
    let d6 = (d14).tan() * (d14).tan();
    let d7 = d3 * (d14).cos() * (d14).cos();
    let d8 = (r * (1.0 - d2)) / (1.0 - d2 * (d14).sin() * (d14).sin()).powf(1.5);

    let d9 = d15 / (d5 * d);
    let mut lat = d14 - ((d5 * (d14).tan()) / d8) * (
        ((d9 * d9) / 2.0 - (((5.0 + 3.0 * d6 + 10.0 * d7) - 4.0 * d7 * d7 - 9.0 * d3) * d9.powi(4)) / 24.0) + (
          ((61.0 + 90.0 * d6 + 298.0 * d7 + 45.0 * d6 * d6) - 252.0 * d3 - 3.0 * d7 * d7) * d9.powi(6)
        ) / 720.0
    );

    lat = (lat * 180.0) / PI;

    let mut lon = (
        (d9 - ((1.0 + 2.0 * d6 + d7) * d9.powi(3)) / 6.0) + (
          ((((5.0 - 2.0 * d7) + 28.0 * d6) - 3.0 * d7 * d7) + 8.0 * d3 + 24.0 * d6 * d6) * d9.powi(5)
        ) / 120.0
    ) / (d14).cos();

    lon = d11 as f64 + (lon * 180.0) / PI;

    LonLat {
        lon, lat,
    }
}

pub fn haversine(p1: &LonLat, p2: &LonLat) -> f64 {
    // convert decimal degrees to radians
    let (lon1, lat1, lon2, lat2) = (
        p1.lon.to_radians(), p1.lat.to_radians(),
        p2.lon.to_radians(), p2.lat.to_radians(),
    );

    // haversine formula
    let dlon = lon2 - lon1;
    let dlat = lat2 - lat1;

    let a = (dlat / 2.0).sin().powi(2) + lat1.cos() * lat2.cos() * (dlon / 2.0).sin().powi(2);
    let c = 2.0 * a.sqrt().asin();

    c * EARTH_RADIUS
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_elevacion_api() {
        let data = vec![
            (-3.82, 1.95, 0.0),
            (-3.76, -1.33, 1.0),
            (1.96, 1.79, 2.0),
            (1.46, -2.71, 3.0),
        ];
        let ele = Elevation::from_lon_lat_z(data);

        // inside bounds
        assert_eq!(ele.altitude([-4.0, 2.0].into()), Some(0.0));
        assert_eq!(ele.altitude([-3.0, -1.0].into()), Some(1.0));
        assert_eq!(ele.altitude([1.0, 1.0].into()), Some(2.0));
        assert_eq!(ele.altitude([1.0, -2.0].into()), Some(3.0));
    }

    #[test]
    fn test_points_from_utm() {
        let data = vec![
            (715587.500, 2157287.500, 1269.740),
            (715597.500, 2157287.500, 1269.650),
            (715602.500, 2157287.500, 1269.640),
            (715607.500, 2157287.500, 1269.620),
        ];
        let ele = Elevation::from_utm(14, data);

        assert_eq!(ele.altitude([-96.94583170115948, 19.49862116395379].into()), Some(1269.74));
        assert_eq!(ele.altitude([-96.94571167230606, 19.49856427532514].into()), Some(1269.650));
        assert_eq!(ele.altitude([-96.94566071033478, 19.498619899762257].into()), Some(1269.640));
        assert_eq!(ele.altitude([-96.94558829069138, 19.49857122838082].into()), Some(1269.620));
    }

    #[test]
    fn test_haversine() {
        let p1 = LonLat::new(-96.91916048526764, 19.52054707977505);
        let p2 = LonLat::new(-96.9186320900917, 19.519844283054393);

        assert!((haversine(&p1, &p2) - 100.0).abs() < 5.0);
    }
}
