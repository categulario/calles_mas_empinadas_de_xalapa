use std::collections::HashMap;
use std::fmt;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Osm3s {
    pub timestamp_osm_base: String,
    pub copyright: String,
}

#[derive(Deserialize, Debug, PartialEq)]
pub struct OsmWay {
    pub id: u64,
    pub nodes: Vec<u64>,
    pub tags: HashMap<String, String>,
}

impl OsmWay {
    pub fn with_id(id: u64) -> OsmWay {
        OsmWay {
            id,
            nodes: Vec::new(),
            tags: HashMap::new(),
        }
    }
}

impl fmt::Display for OsmWay {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(name) = self.tags.get("name") {
            write!(f, "{}", name)
        } else {
            write!(f, "Sin nombre")
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct OsmNode {
    pub id: u64,
    pub lat: f64,
    pub lon: f64,
}

#[derive(Deserialize, Debug)]
#[serde(tag = "type")]
pub enum OsmElement {
    #[serde(rename = "way")]
    Way(OsmWay),

    #[serde(rename = "node")]
    Node(OsmNode),
}

#[derive(Deserialize)]
pub struct OsmResponse {
  pub version: f64,
  pub generator: String,
  pub osm3s: Osm3s,
  pub elements: Vec<OsmElement>,
}
