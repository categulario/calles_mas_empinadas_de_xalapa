use std::fmt;
use std::error;

#[derive(Debug)]
pub enum Error {
    ElevationDataFileNotFound,
    OsmDataFileNotFound,
    OsmDataFileReadError,

    /// Could not parse OSM json data
    OsmDataDecodingError,
    OverpassNetworkError,

    /// Error writing data to osm file
    OverpassIOError,

    /// Problem creating the output file
    OutFileCreationError,
    OutFileWriteError,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::ElevationDataFileNotFound => writeln!(f, "Elevation data file not found"),
            Error::OsmDataFileNotFound => writeln!(f, "OSM data file not found"),
            Error::OsmDataFileReadError => writeln!(f, "OSM data file read error"),
            Error::OsmDataDecodingError => writeln!(f, "Could not decode OSM json data"),
            Error::OverpassNetworkError => writeln!(f, "A network error ocurred while trying to obtain OSM data"),
            Error::OverpassIOError => writeln!(f, "Error writing data to OSM file"),
            Error::OutFileCreationError => writeln!(f, "Error creating the output file"),
            Error::OutFileWriteError => writeln!(f, "Error writing to the output file"),
        }
    }
}

impl error::Error for Error { }
