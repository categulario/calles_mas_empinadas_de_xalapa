use std::collections::HashMap;

use json::JsonValue;

use crate::osm::OsmWay;
use crate::elevation::LonLat;

pub fn feature(way: OsmWay, nodes: &HashMap<u64, LonLat>) -> JsonValue {
    let mut properties = way.tags;

    properties.insert("id".into(), way.id.to_string());

    object!{
      "type": "Feature",
      "properties": properties,
      "geometry": {
        "type": "LineString",
        "coordinates": way.nodes.into_iter().filter_map(|n| nodes.get(&n)).map(|ll| ll.to_v()).collect::<Vec<_>>(),
      }
    }
}

pub fn feature_collection(features: Vec<JsonValue>) -> JsonValue {
    object!{
      "type": "FeatureCollection",
      "features": features,
    }
}
