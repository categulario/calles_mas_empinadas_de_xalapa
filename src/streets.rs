use std::cmp::{Ordering, Reverse};
use std::collections::HashMap;
use std::fmt;

use crate::osm::OsmWay;
use crate::elevation::{Elevation, LonLat, haversine};

#[derive(PartialEq, Copy, Clone, Debug)]
pub struct Slope(f64);

impl Eq for Slope { }

impl PartialOrd for Slope {
    fn partial_cmp(&self, other: &Slope) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl Ord for Slope {
    fn cmp(&self, other: &Slope) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl fmt::Display for Slope {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:.2}%", self.0)
    }
}

pub struct SteepestStreets {
    limit: usize,
    storage: Vec<(Slope, OsmWay)>,
}

impl SteepestStreets {
    pub fn new(num: usize) -> SteepestStreets {
        SteepestStreets {
            limit: num,
            storage: Vec::new(),
        }
    }

    pub fn push(&mut self, steepness: Slope, way: OsmWay) {
        if self.storage.is_empty() {
            // storage is empty, insert the given item
            self.storage.push((steepness, way));
            return;
        }

        let idx = self.storage.binary_search_by_key(&Reverse(steepness), |(s, _w)| Reverse(*s)).unwrap_or_else(|x| x);

        self.storage.insert(idx, (steepness, way));
        self.storage.truncate(self.limit);
    }

    pub fn len(&self) -> usize {
        self.storage.len()
    }

    pub fn into_sorted_vec(self) -> Vec<(Slope, OsmWay)> {
        self.storage
    }
}

pub fn slope(way: &OsmWay, nodes: &HashMap<u64, LonLat>, elevation: &Elevation) -> Option<Slope> {
    way.nodes
        .iter()
        .zip(way.nodes.iter().skip(1))
        .filter_map(|(n1, n2)| {
            let p1 = nodes.get(n1).unwrap();
            let p2 = nodes.get(n2).unwrap();
            let d = haversine(p1, p2);
            let h1 = elevation.altitude(*p1)?;
            let h2 = elevation.altitude(*p2)?;

            Some(Slope((h1 - h2).abs() / d))
        })
        .max()
}

#[cfg(test)]
mod tests {
    use crate::osm::OsmWay;

    use super::*;

    #[test]
    fn test_steepest_streets_api() {
        let mut ss = SteepestStreets::new(5);

        ss.push(Slope(4.0), OsmWay::with_id(4));
        ss.push(Slope(10.0), OsmWay::with_id(10));
        ss.push(Slope(3.0), OsmWay::with_id(3));
        ss.push(Slope(7.0), OsmWay::with_id(7));
        ss.push(Slope(6.0), OsmWay::with_id(6));
        ss.push(Slope(2.0), OsmWay::with_id(2));
        ss.push(Slope(1.0), OsmWay::with_id(1));
        ss.push(Slope(5.0), OsmWay::with_id(5));
        ss.push(Slope(8.0), OsmWay::with_id(8));
        ss.push(Slope(9.0), OsmWay::with_id(9));

        assert_eq!(ss.len(), 5);

        assert_eq!(ss.into_sorted_vec(), vec![
            (Slope(10.0), OsmWay::with_id(10)),
            (Slope(9.0), OsmWay::with_id(9)),
            (Slope(8.0), OsmWay::with_id(8)),
            (Slope(7.0), OsmWay::with_id(7)),
            (Slope(6.0), OsmWay::with_id(6)),
        ]);
    }
}
