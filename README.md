# Las calles más empinadas de xalapa

La idea de este proyecto es encontrar las 13 calles más empinadas de esta
hermosa ciudad. Para calcularlas es necesario contar con datos de elevación y
con información de las calles.

## Cómo correr?

Necesitas [rust](https://rustup.rs), clonar el proyecto y hacer

    cargo test

para ver si las cosas están funcionando, o

    cargo run -- --help

para ver qué parámetros acepta esto en la línea de comandos.

## Datos de elevación

Es necesario contar con uno o varios archivos con información de elevación para
poder obtener resultados. Yo utilizé la carta E14B27D4 del
[INEGI](https://www.inegi.org.mx/temas/relieve/continental/) que está en
resolución 1:10000. El archivo que se necesita tiene extensión `.xyz`.

## Bajar datos de OSM

Para correr el algoritmo se necesitan los datos de elevación y los datos de
calles de Openstreetmap. Para descargarlos se usa el subcomando `overpass`:

    cargo run --release -- overpass --bbox -96.94692,19.49829,-96.88659,19.56372 -o xalapa.osm

## Correr el algoritmo

Una vez conseguida toda la información se procede a correr el algoritmo. Se
necesita proporcionar los datos de elevación y los datos de calles descargados
de OSM, así como el archivo donde se guardará la salida (en formato geojson) y
opcionalmente la cantidad de calles más empinadas a devolver.

    cargo run --release -- explore -e e14b27d4.xyz --osm xalapa.osm -o salida.geojson -n 50
